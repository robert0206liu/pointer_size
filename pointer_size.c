/*
 * =====================================================================================
 *
 *       Filename:  pointer_size.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年11月09日 11時47分41秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>

int main()
{
    int   a;            // An integer
    int  *b;            // A pointer to an integer
    char  c;            // A character
    char *d;            // A pointer to a character
    char  e[100];       // 100 integers
    char *f[100];       // 100 pointers to character
    char (*g)[100];     // A pointer to an array with 100 characters
    char  h[20][5];     // 20*5 integers
    char *i[20][5];     // 20*5 pointers of to character
    char (*j)[20][5];   // A pointer to an array with 20*5 characters

    printf("sizeof(a) = %lu\n", sizeof(a));
    printf("sizeof(b) = %lu\n", sizeof(b));
    printf("sizeof(c) = %lu\n", sizeof(c));
    printf("sizeof(d) = %lu\n", sizeof(d));
    printf("sizeof(e) = %lu\n", sizeof(e));
    printf("sizeof(f) = %lu\n", sizeof(f));
    printf("sizeof(g) = %lu\n", sizeof(g));
    printf("sizeof(h) = %lu\n", sizeof(h));
    printf("sizeof(i) = %lu\n", sizeof(i));
    printf("sizeof(j) = %lu\n", sizeof(j));
    
    return 0;
}
